# 参与PSI的研发

## 概述

PSI是一个商业项目，但其商业模式中有代码开源。

参与PSI研发，就是参与一个商业公司的创业。

## PSI的研发流程

1. issue里面讨论新需求。

2. 编写数据库文档

3. 编写SQL

4. 编写“一键升级数据库”

5. 实现新需求（PHP、JS）

6. 自测

7. 发布代码和更新demo

8. 编写Help文件

9. 用户测试

该流程是传统的软件工程中的“瀑布法”，并没有使用fork-pull这个git流程。

从上述流程可以发现，编写代码只是PSI研发流程中的一部分工作，上述的每一个环节都是参与PSI研发的切入点。

## PSI众包群

PSI使用PSI众包群（QQ群）与外部开发者交流。加入PSI众包群有很高的门槛，需要先用email申请。

有意者可以发email到：crm8000@qq.com
